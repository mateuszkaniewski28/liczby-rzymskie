﻿using System;

namespace LiczbyRzymskie
{
    class Program
    {
        static int zamienNaLiczbe(string r)
        {
            int suma = 0;

            foreach (char c in r.ToCharArray())
            {
                if (c == 'I')
                {
                    suma += 1;
                }
                else if (c == 'V')
                {
                    suma += 5;
                }
                else if (c == 'X')
                {
                    suma += 10;
                }
                else if (c == 'L')
                {
                    suma += 50;
                }
                else if (c == 'C')
                {
                    suma += 100;
                }
                else if (c == 'M')
                {
                    suma += 1000;
                }
            }

            if (r.Contains("IV"))
            {
                suma -= 2;
            }

            if (r.Contains("IX"))
            {
                suma -= 2;
            }

            if (r.Contains("XL"))
            {
                suma -= 20;
            }

            if (r.Contains("XC"))
            {
                suma -= 20;
            }

            if (r.Contains("CM"))
            {
                suma -= 200;
            }

            return suma;
        }

        static void Main(string[] args)
        {
            int num1 = zamienNaLiczbe(args[0]);
            string znak = args[1];
            int num2 = zamienNaLiczbe(args[2]);


            if (znak == "+")
            {
                Console.WriteLine(num1 + num2);
            }
            else if (znak == "-")
            {
                Console.WriteLine(num1 - num2);
            }
            else
            {
                Console.WriteLine("Niepoprawny znak.");
            }
        }
    }
}
